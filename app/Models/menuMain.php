<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class menuMain extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'discription',
        'icon',
        'link',
        'status',
        'menu_access',
    ];

    public function submenus1(){
       return $this->hasMany('App\Models\menuSub1')->where('status','Active')->orderByRaw('id');
    }
}
