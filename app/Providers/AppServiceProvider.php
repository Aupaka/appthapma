<?php

namespace App\Providers;
use App\Models\menuMain;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view)
        {
            $menuMainModel = menuMain::select("*")
            ->where("status", "Active")
            ->orderByRaw("id ASC")
            ->get();
            $view->with('menuMainModel', $menuMainModel);
        });
    }
}
