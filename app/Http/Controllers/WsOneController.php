<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;



class WsOneController extends Controller
{
    public function ws1Dashboard()
    {
        return view('ws1-graph-dashboard');
    }
    public function ws1ManageData()
    {
        return view('ws1-manage-data');
    }
    public function ws1Recovery()
    {
        return view('ws1-recovery');
    }

}

