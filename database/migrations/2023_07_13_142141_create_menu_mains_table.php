<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_mains', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('description',100);
            $table->string('icon',50);
            $table->string('link');
            $table->string('status');
            $table->enum('menu_access', ['Administrator','Admin', 'User', 'General']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_mains');
    }
}
